# Combo Password Web Extension
Use Combo Passwords on almost every website.
- compatible with password managers
- site specific configuration

![example 1](/uploads/fb894ebece29641858abd38e695ed095/image.png)


## Known Bugs
- some sites do not work with enabled overlay
- on some sites the overlay is only half as wide as the pw input
### Site specific
- live.com: pw input gets destroyed? after entering the username



# Development
## Setup
```yarn install```
## Linting
```yarn lint```
## Testing
```yarn test``` combines ```yarn test:unit``` and ```yarn test:validation```
## Development and debugging
- ```yarn start:firefox```
- ```yarn start:firefox-dev``` for the Developer Edition
- or load the project folder (chromium based) or manifest.json (firefox) as temporary extension.
Use```chrome://extensions``` or ```about:debugging``` for firefox.
## Building
- ```yarn build```
## Related Docs
- web-ext [docs](https://extensionworkshop.com/documentation/develop/web-ext-command-reference)
- https://github.com/smooth-code/jest-puppeteer/blob/master/README.md

## Known good sites
- gitlab.com
- github.com
- google.com *no overlay
- cock.li
- wikipedia.org
- twitch.tv
- twitter.com
- facebook.com
