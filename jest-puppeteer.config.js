const path = `${process.env.PWD}/src`; // eslint-disable-line no-undef

module.exports = {
  launch: {
    dumpio: true,
    headless: false,
    args: [`--load-extension=${path}`, `--disable-extensions-except=${path}`],
  },
  browser: 'chromium',
  browserContext: 'default',
};
