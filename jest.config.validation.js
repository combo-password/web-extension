// jest config with jest-puppeteer preset for validation tests
module.exports = {
  verbose: true,

  testMatch: ['**/tests/validation/**/*-spec.js?(x)'],

  preset: 'jest-puppeteer',

  setupFiles: ['./tests/constants.js', './tests/validation/utils.js'],
};
