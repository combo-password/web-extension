module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    webextensions: true,
  },
  plugins: ['prettier'],
  extends: ['eslint:recommended', 'plugin:prettier/recommended'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
  },
  rules: {
    'prettier/prettier': ['error', { singleQuote: true }],
    'no-var': 'error',
    'prefer-const': 'error',
    'no-magic-numbers': [
      'error',
      { ignoreArrayIndexes: true, ignore: [0, 1, -1, 2, -2, 3, -3, 10] },
    ],
  },
};
