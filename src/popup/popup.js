const API = chrome || browser;
const hostSelect = document.getElementById('hostSelect');
const toggleActiveImg = document.getElementById('toggleActiveImg');
const toggleActiveSlider = document.getElementById('toggleActiveSlider');
const toggleOverlaySlider = document.getElementById('toggleOverlaySlider');
const delimiterInput = document.getElementById('delimiter');
const setDelimiterButton = document.getElementById('setDelimiter');

let hostname;
API.tabs.query({ currentWindow: true, active: true }, (result) => {
  hostname = new URL(result[0].url).hostname;
  initOptions();
  initSettings();
});

function initOptions() {
  API.storage.local.get((settings) => {
    let hostOption;
    for (const key in settings) {
      hostOption = document.createElement('option');
      hostOption.value = hostOption.title = hostOption.text = key;
      hostSelect.appendChild(hostOption);
    }

    if (!settings[hostname]) {
      hostOption = document.createElement('option');
      hostOption.value = hostOption.title = hostOption.text = hostname;
      hostSelect.appendChild(hostOption);
    }

    hostSelect.value = hostname;
  });
}

function initSettings() {
  API.storage.local.get((settings) => {
    if (settings[hostname]) {
      toggleActiveSlider.checked = settings[hostname].active;
      toggleOverlaySlider.checked = settings[hostname].overlay;
      toggleActiveImg.src = settings[hostname].active
        ? '../assets/on.png'
        : '../assets/off.png';
      delimiterInput.value = settings[hostname].delimiter;
    } else {
      toggleActiveSlider.checked = settings['global'].active;
      toggleOverlaySlider.checked = settings['global'].overlay;
      toggleActiveImg.src = settings['global'].active
        ? '../assets/on.png'
        : '../assets/off.png';
      delimiterInput.value = settings['global'].delimiter;
    }
  });
}

toggleActiveImg.onclick = toggleActive;
toggleActiveSlider.onclick = toggleActive;
toggleOverlaySlider.onclick = toggleOverlay;
setDelimiterButton.onclick = setDelimiter;
hostSelect.onchange = changeHost;

function changeHost() {
  hostname = hostSelect.value;
  initSettings();
}

function toggleActive(event) {
  if (event.target.id === 'toggleActiveImg') {
    toggleActiveSlider.checked = !toggleActiveSlider.checked;
  }

  toggleActiveImg.src = toggleActiveSlider.checked
    ? '../assets/on.png'
    : '../assets/off.png';

  API.runtime.sendMessage({
    hostname: hostSelect.value,
    message: 'toggleActive',
    value: toggleActiveSlider.checked,
  });
}

function toggleOverlay() {
  API.runtime.sendMessage({
    hostname: hostSelect.value,
    message: 'toggleOverlay',
    value: toggleOverlaySlider.checked,
  });
}

function setDelimiter() {
  API.runtime.sendMessage({
    hostname: hostSelect.value,
    message: 'setDelimiter',
    value: delimiterInput.value,
  });
}
