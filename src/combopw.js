const API = chrome || browser;
const hostname = new URL(window.location.href).hostname;
const ALLOWED_KEYS = /^\S$/m; // one none whitespace character
const FAST_TYPING_TIMEOUT = 111;
const password = [];
const color = [
  '#FF5722',
  '#FF9800',
  '#FFC107',
  '#FFEB3B',
  '#03A9F4',
  '#4CAF50',
  '#009688',
  '#673AB7',
  '#9C27B0',
  '#E91E63',
];

const visualizerContainer = document.createElement('div'); // overlay container on top of default pw input, gets filled with ComboVisualizers
visualizerContainer.id = 'combopw-visualizerContainer';
visualizerContainer.style.cssText =
  'overflow-x: hidden;pointer-events: none; position: absolute; height: auto; pointer-events: none; display: inline-flex; flex-direction: row; flex-wrap: nowrap; margin: inherit; padding: inherit; font: inherit;';
visualizerContainer.tabIndex = -1;

const inputWrapper = document.createElement('div'); // contains overlay container and the default pw input
inputWrapper.id = 'combopw-inputWrapper';
inputWrapper.tabIndex = -1;
inputWrapper.appendChild(visualizerContainer);

const comboVisualizers = [];

let passwordNode;
let buffer = '';
let fastTyping = true; // used to determine if the user is just typing fast or wants to input a key combination
let timeout;
let overlay = true;
let delimiter = '_';

API.storage.local.get((settings) => init(settings));
API.runtime.onMessage.addListener(messageListener);

// handle late injected pw input elments WIP
const observer = new MutationObserver((mutations) =>
  mutations.forEach((mutation) =>
    mutation.addedNodes.forEach((node) => {
      if (node.tagName === 'INPUT' && node.type === 'password') {
        injectComboPw(node);
        return;
      }
      if (node.querySelector) {
        injectComboPw(node.querySelector('input[type=password]'));
      }
    })
  )
);

function init(settings) {
  const testConfig = JSON.parse(
    window.localStorage.getItem('combo-pw-web-extension-test-settings')
  );
  if (testConfig) {
    settings = testConfig;
  }
  if (settings[hostname]) {
    // host specific settings
    overlay =
      settings[hostname].overlay !== undefined
        ? settings[hostname].overlay
        : overlay;
    delimiter =
      settings[hostname].delimiter !== undefined
        ? settings[hostname].delimiter
        : delimiter;

    if (settings[hostname].active) {
      document.querySelectorAll('input[type=password]').forEach((node) => {
        injectComboPw(node);
      });

      observer.observe(document.documentElement, {
        childList: true,
        subtree: true,
      });
    }
  } else {
    // global settings
    overlay = settings['global'].overlay;
    delimiter = settings['global'].delimiter;

    if (settings['global'].active) {
      document.querySelectorAll('input[type=password]').forEach((node) => {
        injectComboPw(node);
      });

      observer.observe(document.documentElement, {
        childList: true,
        subtree: true,
      });
    }
  }
}

function injectComboPw(node) {
  if (passwordNode || !node) return;

  observer.disconnect(); // stop looking for pw input elements

  passwordNode = node;
  passwordNode.value = '';
  passwordNode.onkeydown = keyPressed;
  passwordNode.onkeyup = keyReleased;
  //passwordNode.type = ''; // debug

  if (overlay) {
    injectVisualizer();
  }
}

function injectVisualizer() {
  passwordNode.parentNode.childNodes.in;
  passwordNode.parentNode.insertBefore(inputWrapper, passwordNode);
  inputWrapper.appendChild(passwordNode);

  visualizerContainer.style.paddingTop = window
    .getComputedStyle(passwordNode, null)
    .getPropertyValue('padding-top');
  visualizerContainer.style.paddingRight = window
    .getComputedStyle(passwordNode, null)
    .getPropertyValue('padding-right');
  visualizerContainer.style.paddingBottom = window
    .getComputedStyle(passwordNode, null)
    .getPropertyValue('padding-bottom');
  visualizerContainer.style.paddingLeft = window
    .getComputedStyle(passwordNode, null)
    .getPropertyValue('padding-left');
  visualizerContainer.style.width = passwordNode.clientWidth.toString() + 'px';
  passwordNode.style.color = 'transparent';
}

function messageListener(request) {
  // might handle events without reload again later, check out 283a007d for removed functions
  switch (request.message) {
    case 'toggleActive':
    case 'toggleOverlay':
    case 'setDelimiter':
      location.reload();
      break;
  }
}

function keyPressed(event) {
  const key = event.key;
  const isAllowed = ALLOWED_KEYS.test(key);

  if (!isAllowed && key !== 'Backspace') {
    if (' ' === key) {
      event.preventDefault();
    }
    return;
  }

  event.preventDefault();

  if ('Enter' === key && this.onSubmit) {
    event.target.form.submit();
  } else if ('Backspace' === key && password.length > 0) {
    password.pop();
    removeComboVisualizer(password.length);
    event.target.value = password.join(delimiter);
  } else if (!buffer.includes(key)) {
    buffer += key;
    event.target.value = password.join(delimiter) + buffer;

    if (comboVisualizers.length === password.length + 1) {
      removeComboVisualizer(password.length);
    }
    injectComboVisualizer(buffer, password.length);

    if (buffer.length === 1) {
      // sets fastTyping to false when all keys are pressed long enough
      timeout = setTimeout(() => (fastTyping = false), FAST_TYPING_TIMEOUT);
    }
  }
}

function keyReleased(event) {
  event.preventDefault();
  clearTimeout(timeout);

  if (buffer.length > 0) {
    removeComboVisualizer(password.length);
    if (fastTyping) {
      // all keys in the buffer are treated as single keypress
      for (const combo of buffer) {
        injectComboVisualizer(combo, password.length);
        password.push(combo);
      }
      event.target.value = password.join(delimiter);
    } else {
      // the buffer contains a combo
      injectComboVisualizer(buffer, password.length);
      password.push(buffer);
      event.target.value = password.join(delimiter);

      fastTyping = true; // rearm fastTyping for the next input
    }

    buffer = '';
  }
  fastTyping = true;
}

function injectComboVisualizer(combo, index) {
  if (!overlay) return;

  const comboVisualizer = document.createElement('div');
  comboVisualizer.id = `combo-visualizer-${index}`;
  comboVisualizer.innerText = combo.replace(/./gm, '•');
  comboVisualizer.style.cssText = `z-index: 999;
                                  background-color: ${color[index % 10]};
                                  font-family: "Lucida Console", Monaco, monospace;
                                  border-radius: 6px;padding: 3px;
                                  pointer-events: none;
                                  text-align: center;`;

  comboVisualizers.push(comboVisualizer);

  visualizerContainer.appendChild(comboVisualizer);
  visualizerContainer.scrollTo(visualizerContainer.scrollWidth, 0);
}

function removeComboVisualizer(index) {
  if (!overlay) return;

  visualizerContainer.removeChild(visualizerContainer.childNodes[index]);
  comboVisualizers.splice(index, 1);
}
