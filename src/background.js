const API = chrome || browser;

init();
function init() {
  API.storage.local.get((settings) => {
    if (Object.keys(settings).length === 0) {
      settings['global'] = {
        active: false,
        overlay: true,
        delimiter: '_',
      };
    }

    API.storage.local.set(settings);
  });

  API.tabs.onActivated.addListener(onTabChange); // tab changed
  API.runtime.onMessage.addListener(onMessage);
}

// sets the extension icon
// queues the icon change when the new tab has not finished loading
function onTabChange({ tabId }) {
  API.tabs.get(tabId, (tab) => {
    if (tab.status === 'loading') {
      setIcon('assets/off.png');

      API.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
        if (tab.status == 'complete' && tab.active) {
          setIconByTab(tab);
        }
      });
    } else {
      setIconByTab(tab);
    }
  });
}

// sets the extension icon according to the tab specific settings
function setIconByTab(tab) {
  if (!tab.url) return;
  const hostname = new URL(tab.url).hostname;

  API.storage.local.get((settings) => {
    if (
      (settings[hostname] && settings[hostname].active) ||
      (!settings[hostname] && settings['global'].active)
    ) {
      setIcon('assets/on.png');
    } else {
      setIcon('assets/off.png');
    }
  });
}

function onMessage(request) {
  API.storage.local.get((settings) => {
    if (settings[request.hostname]) {
      switch (request.message) {
        case 'toggleActive':
          settings[request.hostname].active = !settings[request.hostname]
            .active;
          break;
        case 'toggleOverlay':
          settings[request.hostname].overlay = !settings[request.hostname]
            .overlay;
          break;
        case 'setDelimiter':
          settings[request.hostname].delimiter = request.value;
          break;
      }
    } else {
      // int settings
      settings[request.hostname] = {
        active:
          request.message === 'toggleActive'
            ? request.value
            : settings['global'].active,
        overlay:
          request.message === 'toggleOverlay'
            ? request.value
            : settings['global'].overlay,
        delimiter:
          request.message === 'setDelimiter'
            ? request.value
            : settings['global'].delimiter,
      };
    }

    API.storage.local.set(settings);

    API.tabs.query({ currentWindow: true, active: true }, (result) => {
      // todo query for open tabs matching the hostname
      const currentHostname = new URL(result[0].url).hostname;

      if (
        currentHostname === request.hostname ||
        (request.hostname === 'global' && !settings[currentHostname])
      ) {
        if (settings[currentHostname]) {
          setIcon(
            settings[currentHostname].active
              ? 'assets/on.png'
              : 'assets/off.png'
          );
        } else {
          setIcon(
            settings['global'].active ? 'assets/on.png' : 'assets/off.png'
          );
        }

        API.tabs.sendMessage(result[0].id, request); // inform pagescript
      }
    });
  });
}

// set the extension icon
function setIcon(path) {
  API.browserAction.setIcon({ path });
}
