module.exports = {
  verbose: true,

  testMatch: ['**/tests/unit/**/*.[jt]s?(x)'],

  setupFiles: ['./tests/constants.js'],
};
