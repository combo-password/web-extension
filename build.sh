#!/usr/bin/sh

cp LICENSE src/LICENSE
rm -rf web-ext-artifacts
yarn web-ext build --source-dir src
rm src/LICENSE