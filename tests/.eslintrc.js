module.exports = {
  env: {
    jest: true,
    node: true,
  },
  plugins: ['prettier'],
  extends: ['eslint:recommended', 'plugin:prettier/recommended'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    // Puppeteer globals
    page: true,
    browser: true,
    asyncForEach: true,
    sleep: true,
    getPasswordElement: true,
    setConfig: true,
    typeCombos: true,
    FAST_TYPING_DELAY: true,
  },
  parserOptions: {
    ecmaVersion: 2018,
  },
  rules: {
    'prettier/prettier': ['error', { singleQuote: true }],
    'no-var': 'error',
    'prefer-const': 'error',
    'no-magic-numbers': [
      'error',
      { ignoreArrayIndexes: true, ignore: [0, 1, -1, 2, -2, 3, -3, 10] },
    ],
  },
};
