const puppeteer = require('expect-puppeteer'); // eslint-disable-line no-unused-vars
const TESTPAGE = `file://${process.env.PWD}/tests/validation/testpage.html`;

describe.each([
  TESTPAGE,
  'https://github.com/login',
  'https://en.wikipedia.org/w/index.php?title=Special:UserLogin',
])('%s', (website) => {
  beforeEach(async () => {
    await page.goto(website);
    await page.reload({ waitUntil: ['networkidle0', 'domcontentloaded'] });
  });

  it('should input a password normally with default settings', async () => {
    const pwInputElement = await getPasswordElement();
    await pwInputElement.click();

    await page.keyboard.type('abc');
    await typeCombos([['a', 'b', 'c']]);

    const value = await page.evaluate(
      (element) => element.value,
      pwInputElement
    );
    expect(value).toMatch('abcabc');
  });

  it('should input a combo password when the extension is globally active', async () => {
    await setConfig({
      global: {
        active: true,
        overlay: true,
        delimiter: '_',
      },
    });

    const pwInputElement = await getPasswordElement();
    await pwInputElement.click();

    await page.keyboard.type('abc');
    await typeCombos([['a', 'b']]);

    const value = await page.evaluate(
      (element) => element.value,
      pwInputElement
    );
    expect(value).toBe('a_b_c_ab');
  });
  // TODO test fastTyping, delimiter, special characters,
});
