global.sleep = (milliseconds) => {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
};

global.getPasswordElement = async () => {
  return await page.$('input[type=password]');
};

// reset or overwrite the standard config
global.setConfig = async (
  settings = {
    global: {
      active: false,
      overlay: true,
      delimiter: '_',
    },
  }
) => {
  await page.evaluate((settings) => {
    localStorage.setItem(
      'combo-pw-web-extension-test-settings',
      JSON.stringify(settings)
    );
  }, settings);
  await page.reload({ waitUntil: ['networkidle0', 'domcontentloaded'] });
};

global.typeCombos = async (combos) => {
  await asyncForEach(combos, async (keys) => {
    await asyncForEach(keys, async (key) => {
      await page.keyboard.down(key);
    });

    await sleep(FAST_TYPING_DELAY);

    await asyncForEach(keys, async (key) => {
      await page.keyboard.up(key);
    });
  });
};

global.asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};
